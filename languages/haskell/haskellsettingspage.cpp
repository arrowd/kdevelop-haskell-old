/*
    This file is part of KDevelop Haskell Support

    Copyright 2016-2017 Gleb Popov <6yearold@gmail.com>

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may
    be used to endorse or promote products derived from this software without specific
    prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
    IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
    OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "haskellsettingspage.h"

#include <QVBoxLayout>

#include <interfaces/icore.h>
#include <interfaces/iplugincontroller.h>

#include "ui_haskellsettingspage.h"
#include "haskellsettings.h"
#include "haskellutils.h"

HaskellSettingsPage::HaskellSettingsPage(KDevelop::IPlugin* plugin, QWidget* parent)
    : KDevelop::ConfigPage(plugin, HaskellSettings::self(), parent)
{
    QVBoxLayout* l = new QVBoxLayout( this );
    QWidget* w = new QWidget;
    m_Ui = new Ui::HaskellSettingsPage;
    m_Ui->setupUi( w );
    l->addWidget( w );

#ifdef Q_OS_WIN
    m_Ui->kcfg_stackExe->setFilter("*.exe");
    m_Ui->kcfg_hieExe->setFilter("*.exe");
    m_Ui->kcfg_cabalExe->setFilter("*.exe");
#endif

    m_Ui->kcfg_stackExe->setToolTip(HaskellSettings::self()->stackExeItem()->whatsThis());
    m_Ui->stackExeLabel->setToolTip(HaskellSettings::self()->stackExeItem()->whatsThis());

    m_Ui->kcfg_hieExe->setToolTip(HaskellSettings::self()->hieExeItem()->whatsThis());
    m_Ui->hieExeLabel->setToolTip(HaskellSettings::self()->hieExeItem()->whatsThis());

    m_Ui->kcfg_cabalExe->setToolTip(HaskellSettings::self()->cabalExeItem()->whatsThis());
    m_Ui->cabalExeLabel->setToolTip(HaskellSettings::self()->cabalExeItem()->whatsThis());
}

HaskellSettingsPage::~HaskellSettingsPage()
{
    delete m_Ui;
}

QString HaskellSettingsPage::name() const
{
    return i18n("Haskell");
}

QString HaskellSettingsPage::fullName() const
{
    return i18n("Configure Haskell support settings");
}

QIcon HaskellSettingsPage::icon() const
{
    return QIcon::fromTheme("text-x-haskell");
}
