/*
    This file is part of KDevelop Haskell Support

    Copyright 2016-2017 Gleb Popov <6yearold@gmail.com>

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may
    be used to endorse or promote products derived from this software without specific
    prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
    IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
    OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "haskellutils.h"
#include "haskellsettings.h"
#include "projectsettings.h"
#include "haskellsupport.h"

#include <interfaces/iproject.h>
#include <util/path.h>
#include <project/projectmodel.h>

#include <KShell>
#include <QProcess>
#include <QDir>
#include <qdebug.h>

#include <functional>

using namespace KDevelop;

namespace HaskellUtils
{
const char* haskellCfgGroup = "Haskell";

QString findExecutable(QString exe, bool useStack, QString stackExe)
{
    if (useStack) {
        QProcess stackProcess;
        stackProcess.setProgram(stackExe);
        stackProcess.setArguments({ QStringLiteral("path"), QStringLiteral("--bin-path") });
        stackProcess.start(QIODevice::ReadOnly);
        stackProcess.waitForFinished(-1);

        QByteArrayList paths0 = stackProcess.readAllStandardOutput().split(';');
        QStringList paths1;
        std::transform(paths0.begin(), paths0.end(), std::back_inserter(paths1), [](QByteArray a) { return QString::fromLocal8Bit(a); }); //TODO: is fromLocal8Bit ok?

        return QStandardPaths::findExecutable(exe, paths1);
    } else {
        QFileInfo f(exe);

        if (f.isAbsolute() && f.isExecutable())
            return exe;

        return QStandardPaths::findExecutable(exe);
    }
}

bool isConfigured(ProjectData* pd) {
    QDir d(pd->settings->buildDir());

    return d.entryList({ QStringLiteral("setup-config") }).count() > 0;
}

bool createdFromStack(IProject* p)
{
    return p->projectConfiguration()->group("Project")
        .readEntry("CreatedFrom").endsWith(QStringLiteral("stack.yaml"));
}

void setOperationMode(IProject* p, OperationMode m)
{
    getConfigGroup(p).writeEntry("Mode", getModeString(m));
}

KConfigGroup getConfigGroup(IProject* p)
{
    return p->projectConfiguration()->group(haskellCfgGroup);
}

OperationMode getOperationMode(IProject* p)
{
    return getModeString(p) == QStringLiteral("stack") ? StackMode : CabalMode;
}

OperationModes supportedModes(IProject* p)
{
    QDir projectDir(p->path().toLocalFile());
    OperationModes modes;

    if (projectDir.entryList({ QStringLiteral("stack.yaml") }).count() > 0)
        modes |= StackMode;
    if (projectDir.entryList({ QStringLiteral("*.cabal") }).count() > 0)
        modes |= CabalMode;

    return modes;
}

QString getModeString(IProject* p)
{
    return getConfigGroup(p).readEntry("Mode");
}

QString getModeString(OperationMode mode)
{
    return mode == CabalMode ? QStringLiteral("cabal") : QStringLiteral("stack");
}

}
