#include "haskellparsejob.h"

#include <serialization/indexedstring.h>
#include <interfaces/icore.h>
#include <interfaces/ilanguagecontroller.h>
#include <language/interfaces/ilanguagesupport.h>
#include <language/interfaces/iastcontainer.h>
#include <language/backgroundparser/backgroundparser.h>
#include <language/backgroundparser/urlparselock.h>
#include <language/duchain/duchain.h>
#include <language/duchain/duchainlock.h>
#include <language/duchain/builders/abstractdeclarationbuilder.h>
#include <language/duchain/builders/abstractcontextbuilder.h>
#include <language/duchain/duchainutils.h>
#include <language/duchain/duchaindumper.h>

#include "haskellsupport.h"

#include <QReadLocker>

using namespace KDevelop;

HaskellParseJob::HaskellParseJob(const IndexedString &url, ILanguageSupport *languageSupport)
    : KDevelop::ParseJob(url, languageSupport)
{

}

HaskellSupport *HaskellParseJob::haskell() const
{
    return static_cast<HaskellSupport *>(languageSupport());
}

void HaskellParseJob::run(ThreadWeaver::JobPointer self, ThreadWeaver::Thread *thread)
{
    if (abortRequested() || ICore::self()->shuttingDown()) {
        return abortJob();
    }

    qDebug() << "Parse job starting for: " << document().toUrl();

    QReadLocker parseLock(languageSupport()->parseLock()); // kdev-go has it a lot lower

    {
        UrlParseLock urlLock(document());
        readContents();
    }

    if (abortRequested()) {
        return;
    }

    QByteArray code = contents().contents;

    if (!(minimumFeatures() & TopDUContext::ForceUpdate)) {
        DUChainReadLocker lock;
        static const IndexedString langString(haskell()->name());
        for (const ParsingEnvironmentFilePointer &file : DUChain::self()->allEnvironmentFiles(document())) {
            if (file->language() != langString) {
                continue;
            }

            if (!file->needsUpdate() && file->featuresSatisfied(minimumFeatures()) && file->topContext()) {
                qDebug() << "Already up to date, skipping:" << document().str();
                setDuChain(file->topContext());
                if (ICore::self()->languageController()->backgroundParser()->trackerForUrl(document())) {
                    lock.unlock();
                    highlightDUChain();
                }
                return;
            }
            break;
        }
    }

    if (abortRequested()) {
        return;
    }

    //ParseSession session(findParseSessionData(document()));

    //if (!session.data()) {
    //    session.setData(ParseSessionData::Ptr(new ParseSessionData(document(), contents().contents)));
    //}

    //if (abortRequested()) {
    //    return;
    //}

    ReferencedTopDUContext toUpdate = nullptr;
    {
        DUChainReadLocker lock;
        toUpdate = DUChainUtils::standardContextForUrl(document().toUrl());
    }

    if (toUpdate) {
        translateDUChainToRevision(toUpdate);
        toUpdate->setRange(RangeInRevision(0, 0, INT_MAX, INT_MAX));
        toUpdate->clearProblems();
    }

    //session.parse();
    //RustOwnedNode crateNode = RustOwnedNode(node_from_crate(session.crate()));

    ReferencedTopDUContext context;

    if (true) {
        qDebug() << "Parsing succeeded for: " << document().toUrl();

        //DeclarationBuilder builder;
        //builder.setParseSession(&session);
        //context = builder.build(document(), &node, toUpdate);

        setDuChain(context);

        //UseBuilder uses(document());
        //uses.setParseSession(&session);
        //uses.buildUses(&node);

        if (abortRequested()) {
            return;
        }

        highlightDUChain();
    } else {
        qDebug() << "Parsing failed for: " << document().toUrl();

        DUChainWriteLocker lock;
        context = toUpdate.data();

        if (context) {
            ParsingEnvironmentFilePointer parsingEnvironmentFile = context->parsingEnvironmentFile();
            parsingEnvironmentFile->setModificationRevision(contents().modification);
            context->clearProblems();
        } else {
            ParsingEnvironmentFile *file = new ParsingEnvironmentFile(document());
            file->setLanguage(IndexedString("Haskell"));

            context = new TopDUContext(document(), RangeInRevision(0, 0, INT_MAX, INT_MAX), file);
            DUChain::self()->addDocumentChain(context);
        }

        setDuChain(context);
    }

    if (minimumFeatures() & TopDUContext::AST) {
        DUChainWriteLocker lock;
        //context->setAst(IAstContainer::Ptr(session.data()));
    }

    DUChain::self()->emitUpdateReady(document(), duChain());
    qDebug() << "Parse job finished for: " << document().toUrl();
}

