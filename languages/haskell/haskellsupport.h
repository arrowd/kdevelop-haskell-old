/*
    This file is part of KDevelop Haskell Support

    Copyright 2016-2017 Gleb Popov <6yearold@gmail.com>

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may
    be used to endorse or promote products derived from this software without specific
    prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
    IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
    OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef KDEVHASKELLSUPPORT_H
#define KDEVHASKELLSUPPORT_H

#include <interfaces/iplugin.h>
#include <project/interfaces/iprojectfilemanager.h>
#include <project/interfaces/ibuildsystemmanager.h>
#include <language/interfaces/ilanguagesupport.h>
#include <interfaces/iplugin.h>
#include <project/abstractfilemanagerplugin.h>
#include <project/interfaces/iprojectbuilder.h>

#include <QJsonValue>
#include <QProcess>

class ProjectSettings;
class ProjectData
{
public:
    QProcess* hieProcess;
    KDevelop::IProject* project;
    ProjectSettings* settings;
    // used by createFolderItem() to determine which ones are Build
    std::vector<KDevelop::Path> buildFolders;
};

class HaskellSupport
    : public KDevelop::AbstractFileManagerPlugin
    , public KDevelop::ILanguageSupport
    , public KDevelop::IBuildSystemManager
    , public KDevelop::IProjectBuilder
{
Q_OBJECT
Q_INTERFACES( KDevelop::IProjectFileManager )
Q_INTERFACES( KDevelop::ILanguageSupport )
Q_INTERFACES( KDevelop::IBuildSystemManager )
Q_INTERFACES( KDevelop::IProjectBuilder )

public:
    explicit HaskellSupport(QObject *parent, const QVariantList& args = QVariantList());
    ~HaskellSupport() override;

    KDevelop::ProjectFolderItem* import(KDevelop::IProject *project) override;
    KJob* createImportJob(KDevelop::ProjectFolderItem* item) override;

    /** Name Of the Language */
    QString name() const override;

    /** Parsejob used by background parser to parse given url */
    KDevelop::ParseJob *createParseJob(const KDevelop::IndexedString &url) override;

    /** the code highlighter */
    KDevelop::ICodeHighlighting* codeHighlighting() const override;
    KDevelop::BasicRefactoring* refactoring() const override;

    void createActionsForMainWindow(Sublime::MainWindow* window, QString& xmlFile, KActionCollection& actions) override;
    KDevelop::ContextMenuExtension contextMenuExtension(KDevelop::Context* context) override;

    KDevelop::ConfigPage* configPage(int number, QWidget *parent) override;

    int configPages() const override;

    KDevelop::IProjectBuilder* builder() const override;
    KDevelop::Path buildDirectory(KDevelop::ProjectBaseItem*) const override;
    KDevelop::Path::List includeDirectories(KDevelop::ProjectBaseItem *) const override { return KDevelop::Path::List(); }
    QHash<QString, QString> defines(KDevelop::ProjectBaseItem *) const override { return QHash<QString, QString>(); }

    KDevelop::ProjectTargetItem* createTarget(const QString&, KDevelop::ProjectFolderItem*) override { return 0; }
    bool addFilesToTarget(const QList<KDevelop::ProjectFileItem*> &files, KDevelop::ProjectTargetItem* target) override { return false; }
    virtual QList<KDevelop::ProjectTargetItem*> targets() const;
    QList<KDevelop::ProjectTargetItem*> targets(KDevelop::ProjectFolderItem* folder) const override;
    bool removeTarget(KDevelop::ProjectTargetItem*) override { return false; }
    bool removeFilesFromTargets(const QList<KDevelop::ProjectFileItem*> &files) override { return false; }
    KDevelop::Path::List frameworkDirectories(KDevelop::ProjectBaseItem *item) const override { return KDevelop::Path::List(); }
    bool hasBuildInfo(KDevelop::ProjectBaseItem* item) const override { return true; }

    bool isValid(const KDevelop::Path& path, const bool isFolder, KDevelop::IProject* project) const override;
    KDevelop::ProjectFolderItem* createFolderItem(KDevelop::IProject* project, const KDevelop::Path& path, KDevelop::ProjectBaseItem* parent) override;

    KJob* build(KDevelop::ProjectBaseItem *item) override;
    KJob* install(KDevelop::ProjectBaseItem *item, const QUrl &installPrefix) override { return nullptr; };
    KJob* clean(KDevelop::ProjectBaseItem *item) override { return nullptr; };

private:
    QMap<KDevelop::IProject*, ProjectData*> m_projects;
    KDevelop::ICodeHighlighting *m_highlighting;
    KDevelop::BasicRefactoring *m_refactoring;
};

#endif

