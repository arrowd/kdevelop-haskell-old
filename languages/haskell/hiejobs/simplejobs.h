/*
    This file is part of KDevelop Haskell Support

    Copyright 2017 Gleb Popov <6yearold@gmail.com>

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may
    be used to endorse or promote products derived from this software without specific
    prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
    IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
    OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SIMPLEJOBS_H
#define SIMPLEJOBS_H

#include "hiejob.h"

#include <util/path.h>

#include <KJob>

#include <QVector>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

namespace KDevelop
{
class IProject;
}

class IsConfiguredHJ : public HIEJob
{
    Q_OBJECT

public:
    IsConfiguredHJ(KDevelop::IProject* project, QProcess* hieProc,
                   OperationMode mode, const QString & distDir);
    bool isConfigured();

private:
    void handleResponse() override;
    bool m_isConfigured;
};

class ConfigureHJ : public HIEJob
{
    Q_OBJECT

public:
    ConfigureHJ(KDevelop::IProject* project, QProcess* hieProc,
        OperationMode mode, const QString & distDir, const QString & buildTool);

private:
    void handleResponse() override;
};

class BuildHJ : public HIEJob
{
    Q_OBJECT

public:
    BuildHJ(KDevelop::IProject* project, QProcess* hieProc,
        OperationMode mode, KDevelop::Path directory);
    BuildHJ(KDevelop::IProject* project, QProcess* hieProc,
        OperationMode mode, QStringList componentData);

private:
    void handleResponse() override;
};

class IsPreparedHJ : public HIEJob
{
    Q_OBJECT

public:
    IsPreparedHJ(KDevelop::IProject* project, QProcess* hieProc,
        OperationMode mode, const QString & distDir);
    bool isPrepared();

private:
    void handleResponse() override;
    bool m_isPrepared;
};

class PrepareHJ : public HIEJob
{
    Q_OBJECT

public:
    PrepareHJ(KDevelop::IProject* project, QProcess* hieProc,
        OperationMode mode, const QString & distDir, const QString & cabalExe);

private:
    void handleResponse() override;
};

#endif // SIMPLEJOBS_H
