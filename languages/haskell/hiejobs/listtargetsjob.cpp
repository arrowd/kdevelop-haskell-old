/*
    This file is part of KDevelop Haskell Support

    Copyright 2017 Gleb Popov <6yearold@gmail.com>

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors may
    be used to endorse or promote products derived from this software without specific
    prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
    IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
    OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "listtargetsjob.h"
#include "haskellsupport.h"
#include "haskellsettings.h"

#include <interfaces/iproject.h>

#include <project/projectmodel.h>

#include <KShell>
#include <QProcess>
#include <QFileInfo>
#include <qdebug.h>

#include <functional>

using namespace KDevelop;

ListTargetsHJ::ListTargetsHJ(IProject* project, QProcess* proc,
    OperationMode mode, const QString & distDir)
    : HIEJob(project, proc)
{
    QJsonObject params;

    params["mode"] = HIECommand::text(HaskellUtils::getModeString(mode));
    if (!distDir.isEmpty())
        params["distDir"] = HIECommand::file(distDir);

    m_cmd.write("build:listTargets", params);
}

QVector<ListTargetsHJ::Package> ListTargetsHJ::packages()
{
    QVector<Package> ret;
    ret.reserve(m_packages.size());

    ret = std::accumulate(m_packages.begin(), m_packages.end(), ret,
        [](QVector<Package> acc, QJsonValue pv) {
            if (!pv.isObject())
                return acc;
            QJsonObject po = pv.toObject();

            Package p;
            p.name = po["name"].toString();
            p.directory = po["directory"].toString();

            QJsonArray targets = po["targets"].toArray();
            p.targets = std::accumulate(targets.begin(), targets.end(), p.targets,
                [](QVector<Target> acc, QJsonValue tv) {
                    if(!tv.isObject())
                        return acc;
                    QJsonObject to = tv.toObject();

                    Target t;
                    t.name = to["name"].toString();

                    auto type = to["type"].toString();
                    if (type == "setupHs") t.type = Target::SetupHs;
                    if (type == "library") t.type = Target::Library;
                    if (type == "executable") t.type = Target::Executable;
                    if (type == "test") t.type = Target::Test;
                    if (type == "benchmark") t.type = Target::Benchmark;

                    acc.append(t);
                    return acc;
            });

            acc.append(p);
            return acc;
    });

    return ret;
}

void ListTargetsHJ::handleResponse()
{
    if (response.isArray()) {
        m_packages = response.toArray();
    }
    else {
        qDebug() << "ListTargets: response is not an array";
        setError(KJob::UserDefinedError);
    }
    emitResult();
}

#include "moc_listtargetsjob.cpp"
